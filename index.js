const scrape = require('website-scraper');
const PuppeteerPlugin = require('website-scraper-puppeteer');

scrape({
  urls: ['http://handbook.ueh.edu.vn/'],
  directory: '/Users/lanhvo/Documents/scraper-site/test',
  plugins: [
    new PuppeteerPlugin({
      launchOptions: { headless: true } /* optional */,
      scrollToBottom: { timeout: 10000, viewportN: 10 } /* optional */,
      blockNavigation: true /* optional */,
    }),
  ],
});
